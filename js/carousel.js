


$('.owl-carousel').owlCarousel({
    loop:true,
    margin:50,
    nav:false,
    autoplay:100,
    responsive:{
        0:{
            items:1
        },
        610:{
            items:2
        },
        900:{
            items:3
        },
        1200:{
            items:3
        },
    }
});

var owl = $('.owl-carousel');
owl.owlCarousel();
// Go to the next item
$('.next').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
})
